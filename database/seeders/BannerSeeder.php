<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banners')->insert([
            [
                "title" => "iPhone 6 Plus",
                "content" => "It is a long established fact that a reader will be distracted by the readable
                content of a page when looking at its layout",
                "image_url" => "h4-slide.png",
            ],
            [
                "title" => "school supplies & backpacks",
                "content" => "It is a long established fact that a reader will be distracted by the readable
                content of a page when looking at its layout",
                "image_url" => "h4-slide2.png",
            ],
            [
                "title" => "Apple Store Ipod",
                "content" => "It is a long established fact that a reader will be distracted by the readable
                content of a page when looking at its layout",
                "image_url" => "h4-slide3.png",
            ],
            [
                "title" => "Apple Store Ipod",
                "content" => "It is a long established fact that a reader will be distracted by the readable
                content of a page when looking at its layout",
                "image_url" => "h4-slide4.png",
            ],
        ]);
    }
}
