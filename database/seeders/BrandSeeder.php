<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            [
                "name" => "Apple",
                "image_url" => "brand4.png",
                "link" => null,
            ],
            [
                "name" => "Nokia",
                "image_url" => "brand1.png",
                "link" => null,
            ],
            [
                "name" => "Canon",
                "image_url" => "brand2.png",
                "link" => null,
            ],
            [
                "name" => "Samsung",
                "image_url" => "brand3.png",
                "link" => null,
            ],
            [
                "name" => "HTC",
                "image_url" => "brand5.png",
                "link" => null,
            ],
            [
                "name" => "LG",
                "image_url" => "brand6.png",
                "link" => null,
            ],
            [
                "name" => "Sony",
                "image_url" => "sony.png",
                "link" => null,
            ],
        ]);
    }
}
