<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                "name" => "smart phone",
                "active" => 1
            ],
            [
                "name" => "laptop",
                "active" => 1
            ],
            [
                "name" => "PC",
                "active" => 1
            ],
            [
                "name" => "Tablet",
                "active" => 1
            ],
            [
                "name" => "Ti vi",
                "active" => 1
            ]
        ]);
    }
}
