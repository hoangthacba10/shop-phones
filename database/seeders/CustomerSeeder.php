<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->insert([
            [
                "name" => "Chau Ngoc Quang",
                "phone" => "06564926493",
                "email" => "quangcn@gmail.com",
            ],
            [
                "name" => "Nguyen Thi Ngoc Lan",
                "phone" => "0195228351",
                "email" => "lanntn@gmail.com",
            ],
            [
                "name" => "Tran Anh Tuyet",
                "phone" => "0395449651",
                "email" => "tuyetta@gmail.com",
            ],
        ]);
    }
}
