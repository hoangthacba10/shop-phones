<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_images')->insert([
            [
                "product_id" => 1,
                "image_url" => "product1.jpg",
            ],
            [
                "product_id" => 2,
                "image_url" => "product2.jpg",
            ],
            [
                "product_id" => 3,
                "image_url" => "product3.jpg",
            ],
            [
                "product_id" => 4,
                "image_url" => "product4.jpg",
            ],
            [
                "product_id" => 5,
                "image_url" => "product5.jpg",
            ],
        ]);
    }
}
