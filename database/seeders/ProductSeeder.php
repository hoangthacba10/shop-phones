<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            [
                "category_id" => 1,
                "brand_id" => 4,
                "name" => "Samsung Galaxy S22 Ultra",
                "image" => "product-1.jpg",
                "price" => 500,
                "old_price" => 600,
                "description" => "Điện thoại Samsung S22 Ultra phiên bản RAM 12GB cho cảm giác siêu mượt mà
                khi mở và đóng ứng dụng, đi kèm bộ nhớ trong 256GB cho bạn thoải mái lưu trữ những khung hình,
                thước phim chất lượng cao. Vi xử lý Qualcomm Snapdragon 8 Gen 1 hứa hẹn mang đến hiệu năng
                tuyệt đỉnh, xử lý mượt mà mọi tác vụ.",
                "tags" => "Samsung Galaxy, smart phone",
                "is_best_sell" => 1,
                "is_new" => 1,
            ],
            [
                "category_id" => 1,
                "brand_id" => 2,
                "name" => "Nokia G11 Plus",
                "image" => "product-2.jpg",
                "price" => 100,
                "old_price" => 150,
                "description" => "Điện thoại Nokia G11 Plus với thiết kế gọn nhẹ, mặt lưng của điện thoại và
                khung viền của điện thoại được làm bằng chất liệu nhựa, cùng với phiên bản màu Charcoal làm
                tăng sự sang trọng, tinh tế cho chiếc điện thoại hơn. Với chất liệu này sẽ khiến chiếc điện
                thoại Nokia G11 Plus trở nên gọn nhẹ và giản đơn hơn, tạo cảm giác cầm nắm luôn linh hoạt và
                thoải mái trên tay.",
                "tags" => "Nokia, smart phone",
                "is_best_sell" => 1,
                "is_new" => 0,
            ],
            [
                "category_id" => 1,
                "brand_id" => 6,
                "name" => "LG Velvet 5G",
                "image" => "product-3.jpg",
                "price" => 150,
                "old_price" => 200,
                "description" => "Hiệu năng hàng đầu hiện nay khi sở hữu Snapdragon 855,
                Công nghệ mạng 5G thế hệ mới,
                Camera được đầu tư mạnh mẽ,
                Hệ thống âm thanh đỉnh cao,
                Hỗ trợ thêm màn hình độc đáo,
                Màn hình kích thước lớn, siêu nét.",
                "tags" => "Nokia, smart phone",
                "is_best_sell" => 1,
                "is_new" => 0,
            ],
            [
                "category_id" => 1,
                "brand_id" => 7,
                "name" => "LG Velvet 5G",
                "image" => "product-4.jpg",
                "price" => 150,
                "old_price" => 200,
                "description" => "Hiệu năng hàng đầu hiện nay khi sở hữu Snapdragon 855,
                Công nghệ mạng 5G thế hệ mới,
                Camera được đầu tư mạnh mẽ,
                Hệ thống âm thanh đỉnh cao,
                Hỗ trợ thêm màn hình độc đáo,
                Màn hình kích thước lớn, siêu nét.",
                "tags" => "Sony, smart phone",
                "is_best_sell" => 1,
                "is_new" => 1,
            ],
            [
                "category_id" => 1,
                "brand_id" => 1,
                "name" => "Iphone 6 Plus",
                "image" => "product-5.jpg",
                "price" => 150,
                "old_price" => 190,
                "description" => "Apple luôn làm hài lòng tín đồ iFan với các dòng iPhone trong từng
                phân khúc giá. Đặc biệt, phiên bản iPhone 6 vừa ra mắt nhưng đã chiếm lĩnh được thị trường
                smartphone trên toàn thế giới với giá cả phải chăng.",
                "tags" => "Iphone 6, smart phone",
                "is_best_sell" => 1,
                "is_new" => 0,
            ],
        ]);
    }
}
