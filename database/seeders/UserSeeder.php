<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                "username" => "hoangthacba",
                "email" => "hoangpc1208@gmail.com",
                "avatar" => "avata-default",
                "type" => 0,
                'password' => Hash::make('123456'),
            ],
            [
                "username" => "quangvuive",
                "email" => "quangcn@gmail.com",
                "avatar" => "avata-default",
                "type" => 1,
                'password' => Hash::make('123456'),
            ],
            [
                "username" => "tuyetdangyeu",
                "email" => "tuyetta@gmail.com",
                "avatar" => "avata-default",
                "type" => 1,
                'password' => Hash::make('123456'),
            ],
        ]);
    }
}
